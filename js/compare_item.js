(function($)  {
	function comparefunction(context)  {
			var checkbox_id;
			var compare_count = 0;
			var product_type = null;
			
			document.getElementById("btn_compare").disabled = true;
			$("#compare_cart").hide();
			
			if($.cookie(Drupal.settings.basePath + "compare_item_cookie_single") != null) {
				update_cart_on_refresh();
			}
			
			function update_cart_on_refresh(){
				$("#compare_cart").slideDown(500, function(){
					$(this).css("display", "static");
					$(this).css("position", "relative");
					$(this).css("top", "25%");
				});	
				
				compare_item_cookie_single = JSON.parse($.cookie(Drupal.settings.basePath + "compare_item_cookie_single"));
				product_sku = compare_item_cookie_single.sku;
				count = compare_item_cookie_single.count;
				compare_product_type = compare_item_cookie_single.product;
				
				compare_count = parseInt(count);
				
				product_sku_list = product_sku.split(',');
				
				btn_compare_check_enabled();
				$.each(product_sku_list,function(index) {	
							$(".compare-checkbox#"+product_sku_list[index]).attr('checked', true);
							get_item_image(product_sku_list[index],index+1,compare_product_type);
					}
				);
			}
			
			function get_item_image(id,compare_count,product_type) {
				$.ajax({
					type: "POST",
					url: Drupal.settings.basePath + 'compare_item_get_image',
					dataType: 'json',
					data: "id=" + encodeURI(id),
					error: function(jqXHR, textStatus, errorThrown){
					},
					success: function(result){
							var product_title = result["title"];
							var remove_item = Drupal.settings.basePath+'sites/all/modules/compare_item/images/remove.gif';
							
//ADD THE TITLE, IMAGE, REMOVE BUTTON FOR THE ITEM ADDED							
							$('table#compare-cart-table td:nth-child('+compare_count+') div.compare-product-title').html(product_title);
							$('table#compare-cart-table td:nth-child('+compare_count+') div.compare-product-remove').html('<img src="'+remove_item+'"/>');
							$('table#compare-cart-table tr:first-child td:nth-child('+compare_count+')').attr("id",id);
							$('table#compare-cart-table').attr("class",product_type);
							$('table#compare-cart-table tr:first-child td:nth-child('+compare_count+')').css('background-color','black');
							
					}
				});
			}
			
			
			function remove_compare_cart_item(id){
				compare_item_cookie_single = JSON.parse($.cookie(Drupal.settings.basePath + "compare_item_cookie_single"));
				product_sku = compare_item_cookie_single.sku;
				count = compare_item_cookie_single.count;
				compare_product_type = compare_item_cookie_single.product;
				
				compare_cart_table_class = $('table#compare-cart-table').attr("class");
//				alert("compare_product_type = "+compare_product_type+"\ncompare_cart_table_class = "+compare_cart_table_class);
				
				if(compare_cart_table_class == compare_product_type){				
					$('table#compare-cart-table tr:first-child td#'+id).remove();
					$('table#compare-cart-table tr:first-child td:nth-child(3)').after(
						"<td name='compare_item' id='compare_item'>" +
						"	<div class='remove_item compare-product-remove'></div>" +
						"	<div class='compare-product-title compare-product'></div>" +
						"</td>"
					);

//Removing the current id from cookie
				
					product_sku_list = product_sku.split(',');
					
//					alert("LIST before removal = "+product_sku_list+"\nProduct to remove = "+id);
					
					$.each(product_sku_list,function(index) {
//								alert("This value is: "+product_sku_list[index]);
								if (product_sku_list[index] == id) {
									product_sku_list.splice(index,1);
									
//									alert("LIST after removal = "+product_sku_list+"\nProduct to remove = "+id);
					//DECREMENTING THE COUNT VALUE BY 1
									compare_count = parseInt(count);
									compare_count--;
									
									compare_item_cookie_single.sku = product_sku_list.join(',');
									compare_item_cookie_single.count = compare_count;				
					
									$.cookie(Drupal.settings.basePath + "compare_item_cookie_single",JSON.stringify(compare_item_cookie_single),{ path: '/' });
					
									btn_compare_check_enabled();
									
//									alert("The cookie is: "+$.cookie(Drupal.settings.basePath + "compare_item_cookie_single"));
									
									return false;
								}
						}
					);
					
					
	//IF CART IS EMPTY REMOVE THE COOKIE						
					if(compare_count == 0){
						$.cookie(Drupal.settings.basePath + "compare_item_cookie_single",null,{ path: '/' });
						$("#compare_cart").slideUp(100);
						$('table#compare-cart-table').removeAttr("class");
					}
				}
				if(compare_cart_table_class != compare_product_type){
					$(".compare-checkbox").attr('checked', false);
					$("table#compare-cart-table tr:first-child td").each(function(index){
						$(this).attr("id", "compare_item");
						$(this).html(
							"<div class='remove_item compare-product-remove'></div>" +
							"<div class='compare-product-title compare-product'></div>"
						);
						$(this).removeAttr("style");//doesn't work in IE 6,7,8
						$('table#compare-cart-table').removeAttr("class");
					});
					update_cart_on_refresh();
				}
			}
			
			function set_cookie(checkbox_id,product_type){
				var compare_item_cookie_single = new Object();
				product_sku_list = new Array();
				compare_count = 1;
				compare_product_type = product_type;
				
				product_sku_list.push(checkbox_id);
								
				compare_item_cookie_single.sku = product_sku_list.join(',');
				compare_item_cookie_single.count = compare_count;
				compare_item_cookie_single.product = product_type;
				
				$.cookie(Drupal.settings.basePath + "compare_item_cookie_single",JSON.stringify(compare_item_cookie_single),{ path: '/' });				
			}
			function update_cookie(sku,product_type){
				
				compare_item_cookie_single = JSON.parse($.cookie(Drupal.settings.basePath + "compare_item_cookie_single"));
				product_sku = compare_item_cookie_single.sku;
				count = compare_item_cookie_single.count;
				compare_product_type = compare_item_cookie_single.product;
				
				if(compare_product_type != product_type){
					var clear_cart_answer = confirm("The cart contains product of a different type. Do you wish to clear the cart and this item?");
					if(clear_cart_answer == true){
						reset_compare_list();
						set_cookie(sku,product_type);
						$(".compare-checkbox#"+sku).attr('checked', true);
						return true;
					}
					else{
						return false;
					}
				}
				
				
				compare_count = parseInt(count);
				compare_count++;
				product_sku_list = product_sku.split(',');
				product_sku_list.push(sku);
				compare_item_cookie_single.sku = product_sku_list.join(',');
				compare_item_cookie_single.count = compare_count;
				
				$.cookie(Drupal.settings.basePath + "compare_item_cookie_single",JSON.stringify(compare_item_cookie_single),{ path: '/' });
				
				return true;
			}
			
//WHEN THE ADD TO COMPARE OF A ITEM IS CHECKED THIS FUNCTION EXECUTES			
			$(".compare-checkbox").live("click", function(){
				var checkbox_id = $(this).attr("id");
				var checkbox_class = $(this).attr("class").split(" ");
				product_type = checkbox_class[1];
											
				if($(this).is(':checked')) {	
					if($.cookie(Drupal.settings.basePath + "compare_item_cookie_single") == null){
						set_cookie(checkbox_id,product_type);
					}
					else{
						if(!update_cookie(checkbox_id,product_type)){
							return false;
						}
						
					}

					compare_item_cookie_single = JSON.parse($.cookie(Drupal.settings.basePath + "compare_item_cookie_single"));
					count = compare_item_cookie_single.count;
					compare_count = parseInt(count);

//if 4 items are added make all the other checkboxes disabled				
					if (compare_count == 4)	{
						$(".compare-checkbox").attr("disabled", true);
					}
					btn_compare_check_enabled(compare_count);
					
//get the added items image and title using ajax
					get_item_image(checkbox_id,compare_count,product_type);

					$("#compare_cart").slideDown(500, function(){
						$(this).css("display", "static");
						$(this).css("position", "relative");
						$(this).css("top", "25%");
					});	

					
				}
				else {
					remove_compare_cart_item(checkbox_id);
				}
			});

//WHEN THE AN ITEM IS REMOVED FROM THE COMPARE CART THIS FUNCTION EXECUTES			
			$(".compare-product-remove").live('click', function(){
					var id = $(this).parent().attr("id");
					remove_compare_cart_item(id);

					$(".compare-checkbox#"+id).attr('checked', false);
			});
			
			
			function reset_compare_list(){
				compare_count = 0;
				
//Removing the cookies
				$.cookie(Drupal.settings.basePath + "compare_item_cookie_single",null,{ path: '/' });
				
				$(".compare-checkbox").attr('checked', false);
				
				$("table#compare-cart-table tr:first-child td").each(function(index){
					$(this).attr("id", "compare_item");
					$(this).html(
						"<div class='remove_item compare-product-remove'></div>" +
						"<div class='compare-product-title compare-product'></div>"
					);
					$(this).removeAttr("style");//doesn't work in IE 6,7,8
					$('table#compare-cart-table').removeAttr("class");
				});
			}
	
			$(".compare_cart_close").click(function(){
				reset_compare_list();
				btn_compare_check_enabled();
				$("#compare_cart").slideUp(100);
			});
			
			$(".compare_cart_clear").click(function(){
				reset_compare_list();
				btn_compare_check_enabled();
			});
			
			function btn_compare_check_enabled(){
				if (compare_count >= 2) {
					document.getElementById("btn_compare").disabled = false;
				}
				else {
					document.getElementById("btn_compare").disabled = true;
				}
			}
			
			$("#btn_compare").click(function() {
				compare_item_cookie_single = JSON.parse($.cookie(Drupal.settings.basePath + "compare_item_cookie_single"));
				product_sku = compare_item_cookie_single.sku;
				count = compare_item_cookie_single.count;
				compare_product_type = compare_item_cookie_single.product;
				
				var link_compare_page = "http://localhost"+Drupal.settings.basePath+compare_product_type+"/comparepage/";
				var compare_product_sku_list = product_sku;
				link_compare_page += compare_product_sku_list;
				alert(link_compare_page);
				window.location=link_compare_page;
			});
			
	}
	
	Drupal.behaviors.compare = {
			attach: function(context)  {
				comparefunction(context);
			}
	};
})(jQuery);
